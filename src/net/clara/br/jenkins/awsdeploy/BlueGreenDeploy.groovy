package net.clara.br.jenkins.awsdeploy

import com.amazonaws.services.s3.model.AmazonS3Exception
import java.util.concurrent.ConcurrentHashMap
import net.sf.json.JSONObject
import net.clara.br.jenkins.terraform.TerraformRunner

/**
 * Helper for deploy on AWS using blue/green method
 */
class BlueGreenDeploy implements Serializable {
  private static final BLUE_GREEN_DEPLOY_DATA_DIR = 'blue-green-deploy-data'
  private static final STATE_FILE = 'current.json'
  private static final PREVIOUS_STATE_POINTER_FILE = 'previous.json'
  private static final CURRENT_STATE_POINTER_FILE = 'current.json'

  // Holds the deploy data
  private ConcurrentHashMap currentDeployMap = new ConcurrentHashMap()
  private ConcurrentHashMap previousDeployMap = new ConcurrentHashMap()

  /**
   * Constructor
   */
  public BlueGreenDeploy() {}

  public ConcurrentHashMap getCurrentDeploy() {
    return this.currentDeployMap
  }

  public ConcurrentHashMap getPreviousDeploy() {
    return this.previousDeployMap
  }

  /**
   * Check the previous deploy state file on a S3 bucket and bootstrap the
   * internal data.
   *
   * @param steps: The current steps
   * @param awsregion The region where the state bucket was created
   * @param stateBucket The bucket name
   * @param stateBucketKeyBase The path inside the bucket where the state file object is stored
   * @param currentApplicationVersion The application version to deploy
   * @param currentTerraformState The bucket key of Terraform state file object
   */
  public void checkState(
    def steps,
    String awsRegion,
    String stateBucket,
    String stateBucketKeyBase,
    String currentApplicationVersion
  ) {
    String deploy = ''
    ConcurrentHashMap currentDeploy = new ConcurrentHashMap()
    ConcurrentHashMap previousDeploy = new ConcurrentHashMap()
    steps.sh("mkdir -p ${BLUE_GREEN_DEPLOY_DATA_DIR}")

    try {
      steps.dir(BLUE_GREEN_DEPLOY_DATA_DIR) {
        String localDir = steps.pwd()
        String previousDeployPointerfile = "${localDir}/${PREVIOUS_STATE_POINTER_FILE}"
        steps.echo("downloading s3://${stateBucket}/${stateBucketKeyBase}/${STATE_FILE} to ${previousDeployPointerfile}")
        steps.withAWS([region: awsRegion]) {
          steps.s3Download([
              file: previousDeployPointerfile,
              bucket: stateBucket,
              path: "${stateBucketKeyBase}/${STATE_FILE}"
          ])
          steps.echo("Loading previous deploy data from ${previousDeployPointerfile}")
          def json = steps.readJSON([file: previousDeployPointerfile])
          steps.echo("JSON = ${json}")
          previousDeploy.putAll(json)
          steps.echo("Previous deploy: ${previousDeploy}")
        }
      }
      deploy = (previousDeploy['deploy'] == 'blue') ? 'green' : 'blue'
    } catch(AmazonS3Exception exception) {
      if(exception.getMessage() ==~ /.*Error Code: 404 Not Found.*/) {
        steps.echo("No previous deploy found, initializing a new one")
        deploy = 'blue'
      } else {
        throw exception
      }
    }
    currentDeploy.put('deploy', deploy)
    currentDeploy.put('version', currentApplicationVersion)
    steps.echo("Current deploy: ${currentDeploy}")
    this.currentDeployMap.putAll(currentDeploy)
    this.previousDeployMap.putAll(previousDeploy)
  }

  public void saveCurrentState(
    def steps,
    String awsRegion,
    String stateBucket,
    String stateBucketKeyBase
  ) {
    steps.dir(BLUE_GREEN_DEPLOY_DATA_DIR) {
      def json = new JSONObject()
      json.putAll(this.currentDeployMap)
      steps.writeJSON([
        file: CURRENT_STATE_POINTER_FILE,
        json: json
      ])
      steps.withAWS([region: awsRegion]) {
        steps.s3Upload([
            file: CURRENT_STATE_POINTER_FILE,
            bucket: stateBucket,
            path: "${stateBucketKeyBase}/${STATE_FILE}"
        ])
      }
    }
  }

  public void changeDNSEndpoint(
    def steps,
    String environment,
    String application,
    String terraformTool,
    String terraformDNSRepositoryUrl,
    String terraformDNSCodeDirectory,
    String terraformDNSRepositoryBranch,
    String terraformRepositoryCredential,
    String awsRegion,
    String stateBucketKey,
    String terraformStateBucket,
    String stateAwsRegion,
    String terraformDNSVariablesTemplate,
    Boolean changeDNSGradually,
    Boolean dryRun
  ) {
    String blueVersion = (currentDeployMap['deploy'] == 'blue') ? currentDeployMap['version'] : previousDeployMap['version']
    String greenVersion = (currentDeployMap['deploy'] == 'green') ? currentDeployMap['version'] : previousDeployMap['version']
    String blueName = "app-${application}-blue-${environment}-${blueVersion.replaceAll(/\./, '-')}"
    String greenName = (greenVersion != null) ? "app-${application}-green-${environment}-${greenVersion.replaceAll(/\./, '-')}" : ''

    int dnsWeightStep = 0
    if(changeDNSGradually) {
      dnsWeightStep = 10
    } else {
      dnsWeightStep = 100
    }

    int blueWeight = 0
    int greenWeight = 0
    if(currentDeployMap['deploy'] == 'blue') {
      blueWeight = 100
    } else {
      greenWeight = 100
    }

    TerraformRunner dnsEndpointDeployer = new TerraformRunner(steps)
    dnsEndpointDeployer.setConfig([
      tool: terraformTool,
      codeDirectory: terraformDNSCodeDirectory,
      repositoryUrl: terraformDNSRepositoryUrl,
      repositoryBranch: terraformDNSRepositoryBranch,
      repositoryCredential: terraformRepositoryCredential,
      awsRegion: awsRegion,
      stateBucketKey: stateBucketKey,
      stateBucket: terraformStateBucket,
      stateAwsRegion: stateAwsRegion,
      variablesTemplate: terraformDNSVariablesTemplate,
      variablesTemplateBinding: [
        application: application,
        blueName: blueName,
        blueVersion: blueVersion,
        blueWeight: blueWeight,
        greenName: greenName,
        greenVersion: greenVersion,
        greenWeight: greenWeight
      ]
    ])
    dnsEndpointDeployer.prepareForRun()
    dnsEndpointDeployer.run(dryRun ? TerraformRunner.ACTION_PLAN : TerraformRunner.ACTION_APPLY)
  }
}
