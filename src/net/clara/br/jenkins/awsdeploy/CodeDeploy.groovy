package net.clara.br.jenkins.awsdeploy

import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.GetObjectRequest
import com.amazonaws.services.s3.model.S3Object
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.codedeploy.AmazonCodeDeployClient
import com.amazonaws.services.codedeploy.model.S3Location
import com.amazonaws.services.codedeploy.model.BundleType
import com.amazonaws.services.codedeploy.model.RevisionLocation
import com.amazonaws.services.codedeploy.model.RevisionLocationType
import com.amazonaws.services.codedeploy.model.RegisterApplicationRevisionRequest
import com.amazonaws.services.codedeploy.model.CreateDeploymentResult
import com.amazonaws.services.codedeploy.model.CreateDeploymentRequest
import com.amazonaws.services.codedeploy.model.GetDeploymentRequest
import com.amazonaws.services.codedeploy.model.DeploymentInfo
import com.amazonaws.services.codedeploy.model.DeploymentStatus

class CodeDeploy implements Serializable {
  // Constructor
  public CodeDeploy() { }

  // Retrieve a CodeDeploy S3 revision location
  private RevisionLocation getRevisionS3Location(
    String deployBucket,
    String artifactFile
  ) {
    AmazonS3Client s3 = new AmazonS3Client()
    GetObjectRequest s3Request = new GetObjectRequest(deployBucket, artifactFile)
    S3Object s3Artifact = s3.getObject(s3Request)

    // Setup the location for the
    S3Location s3Location = new S3Location()
    s3Location.setBucket(deployBucket)
    s3Location.setKey(artifactFile)
    s3Location.setBundleType(BundleType.Zip)
    s3Location.setETag(s3Artifact.getObjectMetadata().getETag())

    // Create the revision location
    RevisionLocation revisionLocation = new RevisionLocation()
    revisionLocation.setRevisionType(RevisionLocationType.S3)
    revisionLocation.setS3Location(s3Location)

    return revisionLocation
  }

  // Check for the deploy status periodically for the end of deploy
  private String waitForDeploy(def steps, AmazonCodeDeployClient codedeploy, String deploymentId, Integer timeout) {
    GetDeploymentRequest deployInfoRequest = new GetDeploymentRequest();
    steps.echo("waiting for deploy id ${deploymentId} to complete")
    deployInfoRequest.setDeploymentId(deploymentId);

    DeploymentInfo deployStatus = codedeploy.getDeployment(deployInfoRequest).getDeploymentInfo();
    while(deployStatus.getCompleteTime() == null) {
        steps.echo("status: ${deployStatus.getStatus()}...")
        Thread.sleep(1000)
        deployStatus = codedeploy.getDeployment(deployInfoRequest).getDeploymentInfo();
    }

    steps.echo("Deploy completed with status: " + deployStatus.getStatus())
    return deployStatus.getStatus()
  }

  /*
   * Register a new application revision  stored on a S3 bucket on a given CodeDeploy application
   *
   * @param: steps The Jenkins pipeline step instance
   * @param: codedeployAppName The CodeDeploy aplication name
   * @param: deployBucket The S3 bucket where the artifact is stored
   * @param: artifactFile The S3 bucket key point to the artifact object name to deploy
   *
   */
  public void registerS3Revision(
    def steps,
    String codedeployAppName,
    String deployBucket,
    String artifactFile
  ) {
    AmazonCodeDeployClient codedeploy = new AmazonCodeDeployClient()

    steps.echo("Registering revision for artifact s3://${deployBucket}/${artifactFile}")
    codedeploy.registerApplicationRevision(
        new RegisterApplicationRevisionRequest()
            .withApplicationName(codedeployAppName)
            .withRevision(getRevisionS3Location(deployBucket, artifactFile))
            .withDescription("Registered by Jenkins")
    )
  }

  /*
   * Create an In-place deployment from a S3 bucket application artifact object.
   *
   * @param: steps The Jenkins pipeline step instance
   * @param: codedeployAppName The CodeDeploy aplication name
   * @param: deployGroup The CodeDeploy Application group to receive the deploy
   * @param: deployBucket The S3 bucket where the artifact is stored
   * @param: artifactFile The S3 bucket key point to the artifact object name to deploy
   *
   * @return: String with deploy status
   */
  public String deployInPlaceFromS3(
    def steps,
    String codedeployAppName,
    String deployGroup,
    String deployBucket,
    String artifactFile,
    Integer timeoutInSecs
  ) {
    steps.echo("Creating deployment on '${codedeployAppName}:${deployGroup}' from s3://${deployBucket}/${artifactFile}")
    AmazonCodeDeployClient codedeploy = new AmazonCodeDeployClient()

    CreateDeploymentResult deployResult = codedeploy.createDeployment(
        new CreateDeploymentRequest()
            .withDeploymentGroupName(deployGroup)
            .withApplicationName(codedeployAppName)
            .withRevision(getRevisionS3Location(deployBucket, artifactFile))
            .withDescription("Deployment created by Jenkins")
    )
    String finalStatus = waitForDeploy(steps, codedeploy, deployResult.deploymentId, timeoutInSecs * 1000)

    return finalStatus
  }
}
